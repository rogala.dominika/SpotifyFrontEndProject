import { Component, OnInit } from '@angular/core';
import { SearchDataService } from '../services/search-data.service';
import { SearchService } from '../services/song.service';
import { ISearchResult } from '../models/searchResult';
import { MatSnackBar } from '@angular/material';
import { ISearchDetails } from '../models/searchDetails';
import * as moment from 'moment';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {

  displaySpinner: boolean;

  // searchInput: number;
  categories: string[] = ['artist', 'track', 'album'];
  category: string;
  searchData: ISearchResult[];    // do wyświetlania na stronie głownej
  view: string; // żeby switch nie bindowal sie od razu po zmianie checkboxa tylko po kliknieciu enter w wyszukiwarce
  detailsVisible: number;
  searchDetails: ISearchDetails;

  constructor(
    private searchService: SearchService,
    private searchDataService: SearchDataService,
    public snackBar: MatSnackBar) { }

  ngOnInit() {
    this.displaySpinner = false;
    this.detailsVisible = null;
  }

  getData(search: string): void {
    this.searchDataService.searchData = null;
    this.searchData = null;
    this.view = null;

    this.displaySpinner = true;
    this.searchService.getData(search).subscribe(
      data => {
        this.view = this.searchDataService.searchCategory;
        this.searchData = data;
      },
      error => {
        this.displaySpinner = false;
        this.snackBar.open("This search doesn't exist", 'OK', { duration: 3000 });
      },
      () => this.displaySpinner = false
    );
  }


  addToFavorites(id: string): void {
    const dataToFavorites = this.searchData.find(x => x.id === id);

    this.searchService.postDataToFavourites(dataToFavorites).subscribe(
      data => { },
      error => {
        if (error.error.text = 'artist inserted') {
          this.openSnackBar(dataToFavorites);
        } else {
          console.error(error);
        }
      }
    );
  }

  openSnackBar(data: ISearchResult): void {
    let message: string;

    switch (this.searchDataService.searchCategory) {
      case 'album':
        message = data.albumName;
        break;
      case 'artist':
        message = data.artistName;
        break;
      case 'track':
        message = data.trackName;
        break;
    }

    this.snackBar.open(`${message} added to favorites`, 'OK', { duration: 3000 });
  }

  getDetails(id: string, index: number): void {
    if (this.detailsVisible === null) {

      this.searchService.getDetails(id).subscribe(
        data => {
          if (data !== null) {
            this.searchDetails = data;
            this.detailsVisible = index;
          } else {
            this.snackBar.open(`Failed to load track's details`, 'OK', { duration: 3000 });
          }
        },
        error => {
          console.error(error);
        }
      );
    } else if (this.detailsVisible === index) {
      this.detailsVisible = null;
    } else {
      // this.detailsVisible = null;
      this.detailsVisible = index;
    }
  }
}


