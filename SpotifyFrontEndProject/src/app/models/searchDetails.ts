export interface ISearchDetails {
    id: number;
    name: string;
    type: string;
    uri: string;
    albums: Array<IAlbum>;
}


interface ITrack {
    id: number;
    name: string;
    popularity: number;
    trackNumber: number;
    isExplicit: boolean;
    isPlayable: boolean;
    type: string;
    durationMs: number;
    discNumber: number;
}

interface IAlbum {
    id: number;
    name: string;
    type: string;
    uri: string;
    tracks: Array<ITrack>;
}
