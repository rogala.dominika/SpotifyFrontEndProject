export interface ISearchResult {
    artistName: string;
    id: string;
    trackName: string;
    albumName: string;
}
