export class SearchAdvanced {
    name: string;
    popularityLower: number;
    popularityGreater: number;
    trackNumberLower: number;
    trackNumberGreater: number;
    isExplicit: boolean;
    isPlayable: boolean;
    durationMsLower: number;
    durationMsGreater: number;
    diskNumberLower: number;
    discNumberGreater: number;

    constructor(n: string, popL: number, popG: number, trL: number, trG: number,
        expl: boolean, play: boolean, durL: number, durG: number, diskL: number,
        diskG: number) {
        this.name = n;
        this.popularityLower = popL;
        this.popularityGreater = popG;
        this.trackNumberLower = trL;
        this.trackNumberGreater = trG;
        this.isExplicit = expl;
        this.isPlayable = play;
        this.durationMsLower = durL;
        this.durationMsGreater = durG;
        this.diskNumberLower = diskL;
        this.discNumberGreater = diskG;
    }
}
