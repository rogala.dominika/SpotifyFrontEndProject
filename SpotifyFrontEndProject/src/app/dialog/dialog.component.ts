import { Component, OnInit, Output, Input } from '@angular/core';
import { EventEmitter } from '@angular/core';
import { SearchService } from '../services/song.service';

@Component({
  selector: 'app-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.scss']
})
export class DialogComponent implements OnInit {

  @Output() popupClosed: EventEmitter<boolean> = new EventEmitter();
  @Input() popupHeader: string;
  @Input() popupBody: string;
  @Input() popupButtonVisible: boolean;
  @Input() closeVisible: boolean;

  constructor(private searchService: SearchService) { }

  ngOnInit() {
  }

  closePopup(): void {
    this.popupClosed.emit(false);
  }


}
