import { Component } from '@angular/core';
import { DialogComponent } from './dialog/dialog.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
  popupVisible: boolean;

  constructor() {};

  ngOnInit(): void {
    this.popupVisible = true;
  }

}

