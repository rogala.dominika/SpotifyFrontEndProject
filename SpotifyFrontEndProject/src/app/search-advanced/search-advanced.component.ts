import { Component, OnInit } from '@angular/core';
import { ISearchResult } from '../models/searchResult';
import { SearchService } from '../services/song.service';
import { SearchAdvanced } from '../models/searchAdvanced';
import { ISearchDetails } from '../models/searchDetails';

@Component({
  selector: 'app-search-advanced',
  templateUrl: './search-advanced.component.html',
  styleUrls: ['./search-advanced.component.scss']
})
export class SearchAdvancedComponent implements OnInit {

  tracks: ISearchResult[];
  detailsVisible: number;
  searchDetails: ISearchDetails;

  constructor(private searchService: SearchService) { }

  ngOnInit() {
    this.detailsVisible = null;
  }

  getTracks(): void { }

  getData(
    name: string, popularityL: number, popularityG: number, trackL: number,
    trackG: number, durationL: number, durationG: number, diskL: number,
    diskG: number, explicit: boolean, playable: boolean): void {
    const search = new SearchAdvanced(name, popularityL, popularityG, trackL, trackG,
      explicit, playable, durationL, durationG, diskL, diskG);

    this.searchService.getAdvancedTracks(search).subscribe(
      data => {
        this.tracks = data;
      },
      error => {
        console.error(error);
      }
    );
  }

  getDetails(id: string, index: number): void {
    if (this.detailsVisible === null) {

      this.searchService.getDetails(id).subscribe(
        data => {
          this.searchDetails = data;
          this.detailsVisible = index;
        },
        error => {
          console.error(error);
        }
      );
    } else {
      this.detailsVisible = null;
    }
  }

  addToFavorites(id: string): void{
    const dataToFavorites = this.tracks.find(x => x.id === id);

    this.searchService.postDataToFavourites(dataToFavorites).subscribe(
      data => {
        // this.openSnackBar();
      },
      error => {
        console.error(error);
      }
    );
  }
}
