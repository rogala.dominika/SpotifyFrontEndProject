import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { HttpClientModule } from '@angular/common/http';
// import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';
// import { InMemoryDataService } from './services/in-memory-data.service';
import { AppRoutingModule } from './app-routing.module';

import { MatRadioModule, MatSnackBarModule } from '@angular/material';

import { AppComponent } from './app.component';
import { NavBarComponent } from './nav-bar/nav-bar.component';
import { SearchComponent } from './search/search.component';
import { DialogComponent } from './dialog/dialog.component';
import { FavoritesComponent } from './favorites/favorites.component';
import { SnackBarComponent } from './snack-bar/snack-bar.component';
import { SpinnerComponent } from './spinner/spinner.component';
import { SearchAdvancedComponent } from './search-advanced/search-advanced.component';
import { HomeComponent } from './home/home.component';
import { HttpModule } from '@angular/http';
import { RoutingGuardService } from './services/routing-guard.service';

import { CookieService } from 'ngx-cookie-service';

@NgModule({
  declarations: [
    AppComponent,
    NavBarComponent,
    SearchComponent,
    SpinnerComponent,
    DialogComponent,
    FavoritesComponent,
    SnackBarComponent,
    SearchAdvancedComponent,
    HomeComponent,
  ],
  imports: [
    MatRadioModule,
    MatSnackBarModule,
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
    HttpModule,
    // HttpClientInMemoryWebApiModule.forRoot(
    //   InMemoryDataService, { dataEncapsulation: false, delay: 1000 }
    // ),
    AppRoutingModule
  ],
  entryComponents: [SnackBarComponent],
  providers: [RoutingGuardService, CookieService],
  bootstrap: [AppComponent]
})
export class AppModule { }
