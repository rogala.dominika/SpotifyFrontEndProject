import { NgModule, Component } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SearchComponent } from './search/search.component';
import { FavoritesComponent } from './favorites/favorites.component';
import { SearchAdvancedComponent } from './search-advanced/search-advanced.component';
import { HomeComponent } from './home/home.component';
import { RoutingGuardService } from './services/routing-guard.service';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', component: HomeComponent },
  {
    path: 'simplesearch',
    component: SearchComponent,
    canActivate: [RoutingGuardService]
  },
  {
    path: 'advancedsearch',
    component: SearchAdvancedComponent,
    canActivate: [RoutingGuardService]
  },
  {
    path: 'favorites',
    component: FavoritesComponent,
    canActivate: [RoutingGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

