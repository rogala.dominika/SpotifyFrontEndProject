import { Component, OnInit } from '@angular/core';
import { SearchService } from '../services/song.service';
import { ISearchResult } from '../models/searchResult';
import { SearchDataService } from '../services/search-data.service';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-favorites',
  templateUrl: './favorites.component.html',
  styleUrls: ['./favorites.component.scss']
})
export class FavoritesComponent implements OnInit {

  favorites: ISearchResult[];
  displaySpinner: boolean;
  fileToUpload: File = null;

  constructor(private searchService: SearchService, private searchDataService: SearchDataService, public snackBar: MatSnackBar) { }

  ngOnInit() {
    this.getFavorites();
  }

  getFavorites(): void {

    this.displaySpinner = true;

    this.searchService.getFavorites(this.searchDataService.email).subscribe(
      data => {
        this.favorites = data;
      },
      error => {
        console.error(error);
      },
      () => {
        this.displaySpinner = false;
      }
    );
  }

  exportFile(): void {
    this.searchService.exportFile().subscribe(
      data => {
        this.downloadFile(data);
      },
      error => {
        if (error.status === 404) {
          this.openSnackBar('Database is empty');
        }
      }
    );
  }

  downloadFile(data: any) {
    const blob = new Blob([(data)], { type: 'application/octet-stream' });
    const url = window.URL.createObjectURL(blob);
    window.open(url);
  }

  handleFileInput(files: FileList) {
    this.fileToUpload = files.item(0);
    this.importFile();
  }

  importFile(): void {
    this.searchService.importFile(this.fileToUpload).subscribe(
      data => {
        this.openSnackBar('File imported');
      }, error => {
        if (error.status === 409) {
          this.openSnackBar('Import failed, database must be empty.');
        } else {
          console.error(error);
        }
      });
  }

  openSnackBar(message: string): void {
    this.snackBar.open(`${message}`, `OK`, { duration: 3000 });
  }

}
