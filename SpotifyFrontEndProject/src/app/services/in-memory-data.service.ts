import { InMemoryDbService } from 'angular-in-memory-web-api';

export class InMemoryDataService implements InMemoryDbService {
  createDb() {
    // chyba tak ma ta baza wyglądać
    const details = [
      {
        id: 1, name: 'Krzysztof Krawczyk', type: 'artist', uri: 'xyz', albums: [
          {
            id: 1, name: 'Życie jak wino', type: 'album', uri: 'xxx', tracks: [
              {
                id: 1, name: 'Paryz i my', popularity: 91, trackNumber: 3
              }
            ]
          }
        ]
      },
      {
        id: 2, name: 'Krzysztof Krawczyk', type: 'artist', uri: 'xyz', albums: [
          {
            id: 1, name: 'Życie jak wino', type: 'album', uri: 'xxx', tracks: [
              {
                id: 1, name: 'Paryz i my', popularity: 41, trackNumber: 5
              }
            ]
          }
        ]
      },
      {
        id: 3, name: 'Krzysztof Krawczyk', type: 'artist', uri: 'xyz', albums: [
          {
            id: 1, name: 'Życie jak wino', type: 'album', uri: 'xxx', tracks: [
              {
                id: 1, name: 'Paryz i my', popularity: 95, trackNumber: 13
              }
            ]
          }
        ]
      },
      {
        id: 4, name: 'Krzysztof Krawczyk', type: 'artist', uri: 'xyz', albums: [
          {
            id: 1, name: 'Życie jak wino', type: 'album', uri: 'xxx', tracks: [
              {
                id: 1, name: 'Paryz i my', popularity: 81, trackNumber: 23
              }
            ]
          }
        ]
      }
    ];


    const artists = [
      { id: 1, trackName: null, artistName: 'Krzysztof Krawczyk', albumName: null },
      { id: 4, trackName: null, artistName: 'Golec Uorkiestra', albumName: null },
      { id: 6, trackName: null, artistName: 'Maryla Rodowicz', albumName: null },
      { id: 8, trackName: null, artistName: 'Urszula', albumName: null },
      { id: 9, trackName: null, artistName: 'Bajm', albumName: null }
    ];


    const albums = [
      { id: 1, trackName: null, artistName: 'Krzysztof Krawczyk', albumName: 'Życie jak wino' },
      { id: 2, trackName: null, artistName: 'Krzysztof Krawczyk', albumName: 'Pol wieku człowieku!' },
      { id: 3, trackName: null, artistName: 'Krzysztof Krawczyk', albumName: 'Duety', },
      { id: 4, trackName: null, artistName: 'Krzysztof Krawczyk', albumName: 'Biało-Czerwoni! Przeboje kibica', }
    ];

    const tracks = [
      { id: 1, trackName: 'Zycie jak wino', artistName: 'Krzysztof Krawczyk', albumName: 'Życie jak wino' },
      { id: 2, trackName: 'Europo. nie mozesz zyc bez Boga', artistName: 'Krzysztof Krawczyk', albumName: 'Życie jak wino' },
      { id: 3, trackName: 'Paryz i my', artistName: 'Krzysztof Krawczyk', albumName: 'Życie jak wino' },
      { id: 4, trackName: 'Czy to jest kraj dla starych ludzi', artistName: 'Krzysztof Krawczyk', albumName: 'Życie jak wino' },
      { id: 5, trackName: 'Gdy nam śpiewał Elvis Presley', artistName: 'Krzysztof Krawczyk', albumName: 'Życie jak wino' },
      { id: 6, trackName: 'Parostatek', artistName: 'Krzysztof Krawczyk', albumName: 'The very best of Vol. 1' },
      { id: 1, trackName: 'Zycie jak wino', artistName: 'Krzysztof Krawczyk', albumName: 'Życie jak wino' },
      { id: 2, trackName: 'Europo. nie mozesz zyc bez Boga', artistName: 'Krzysztof Krawczyk', albumName: 'Życie jak wino' },
      { id: 3, trackName: 'Paryz i my', artistName: 'Krzysztof Krawczyk', albumName: 'Życie jak wino' },
      { id: 4, trackName: 'Czy to jest kraj dla starych ludzi', artistName: 'Krzysztof Krawczyk', albumName: 'Życie jak wino' },
      { id: 5, trackName: 'Gdy nam śpiewał Elvis Presley', artistName: 'Krzysztof Krawczyk', albumName: 'Życie jak wino' },
      { id: 6, trackName: 'Parostatek', artistName: 'Krzysztof Krawczyk', albumName: 'The very best of Vol. 1' },
    ];

    const favorites = [];

    return { tracks, favorites, albums, artists, details };
  }

}
