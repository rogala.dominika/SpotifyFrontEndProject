import { Injectable } from '@angular/core';
import { SearchDataService } from './search-data.service';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { Observable } from '../../../node_modules/rxjs';

@Injectable()
export class RoutingGuardService implements CanActivate {

  constructor(private searchDataService: SearchDataService) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    if (this.searchDataService.email !== null && this.searchDataService.email !== 'User not logged in') {
      return true;
    } else {
      return false;
    }
  }
}