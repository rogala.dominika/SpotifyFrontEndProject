import { Injectable } from '@angular/core';
import { Observable, ObservableLike } from 'rxjs';
import { HttpClient, HttpHeaders, HttpResponse, HttpClientModule } from '@angular/common/http';
import { ISearchResult } from '../models/searchResult';
import { ISearchDetails } from '../models/searchDetails';
import { SearchDataService } from './search-data.service';
import { tap, map, catchError } from 'rxjs/operators';
import { SearchAdvanced } from '../models/searchAdvanced';
import { ResponseContentType } from '@angular/http';

@Injectable({
  providedIn: 'root'
})
export class SearchService {

  private apiGetArtists = 'http://localhost:51724/search/artist?nameofartist';
  private apiGetAlbums = 'http://localhost:51724/search/album?nameofalbum';
  private apiGetTracks = 'http://localhost:51724/search/track?nameoftrack';
  private apiGetAdvancedTracks = 'http://localhost:51724/advancedsearch/track';
  private apiAddToFavorites = 'http://localhost:51724/insert';
  private apiGetFavorites = 'http://localhost:51724/get/collection?email';

  private apiGetTrackDetails = 'http://localhost:51724/search/trackdetails?id';

  private apiExportFile = 'http://localhost:51724/database/export';
  private apiImportFile = 'http://localhost:51724/database/import';

  private apiCheckIfLoged = 'http://localhost:51724/emailsession';
  private apiLogOut= 'http://localhost:51724/logoutsession';
  private apiOutLog = 'http://localhost:51724/outlog';

  private headers = new Headers();


  constructor(private http: HttpClient, private searchService: SearchDataService) {
  }



  getData(search: string): Observable<ISearchResult[]> {
    if (this.searchService.searchCategory === 'artist') {
      return this.http.get<ISearchResult[]>(`${this.apiGetArtists}=${search}`);
    } else if (this.searchService.searchCategory === 'album') {
      return this.http.get<ISearchResult[]>(`${this.apiGetAlbums}=${search}`);
    } else {
      return this.http.get<ISearchResult[]>(`${this.apiGetTracks}=${search}`);
    }

  }

  getDetails(id: string): Observable<ISearchDetails> {
    return this.http.get<ISearchDetails>(`${this.apiGetTrackDetails}=${id}`);
  }

  postDataToFavourites(searchResult: ISearchResult): Observable<ISearchResult> {
    if (this.searchService.searchCategory === 'artist') {
      return this.http.post<ISearchResult>(`${this.apiAddToFavorites}/artist`, searchResult);
    } else if (this.searchService.searchCategory === 'album') {
      return this.http.post<ISearchResult>(`${this.apiAddToFavorites}/album`, searchResult);
    } else {
      return this.http.post<ISearchResult>(`${this.apiAddToFavorites}/track`, searchResult);
    }
  }

  getAdvancedTracks(track: SearchAdvanced): Observable<ISearchResult[]> {
    return this.http.post<ISearchResult[]>(this.apiGetAdvancedTracks, track);
  }

  getFavorites(email: string): Observable<ISearchResult[]> {
    return this.http.get<ISearchResult[]>(`${this.apiGetFavorites}=${email}`);
  }

  exportFile(): Observable<any> {
    return this.http.get(this.apiExportFile, { responseType: 'text' }).pipe(map(res => { return res; }));
  }

  importFile(file: File): Observable<File> {
    const _formData = new FormData();
    _formData.append('file', file, file.name);
    return this.http.post<any>(this.apiImportFile, _formData);

  }

  checkIfLoged(): Observable<string> {
    return this.http.get(this.apiCheckIfLoged, { responseType: 'text' }).pipe(map(res => { return res; }));
  }

  logOut(): Observable<string> {
    return this.http.get(this.apiLogOut, { responseType: 'text' }).pipe(map(res => { return res; }));
  }

  outLog(): Observable<any> {
    return this.http.get<any>(this.apiOutLog);
  }



}
