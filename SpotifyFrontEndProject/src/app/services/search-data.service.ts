import { Injectable } from '@angular/core';
import { ISearchResult } from '../models/searchResult';

@Injectable({
  providedIn: 'root'
})
export class SearchDataService {

  searchData: ISearchResult[];
  searchCategory: string;
  email: string;

  constructor() {
    this.searchCategory = 'artist';
    this.email = 'User not logged in';
  }
}
