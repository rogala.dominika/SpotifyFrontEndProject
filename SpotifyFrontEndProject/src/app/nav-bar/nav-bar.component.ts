import { Component, OnInit } from '@angular/core';
import { SearchDataService } from '../services/search-data.service';
import { CookieService } from '../../../node_modules/ngx-cookie-service';
import { SearchService } from '../services/song.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.scss']
})
export class NavBarComponent implements OnInit {

  displayPopup: boolean;

  constructor(public searchDataService: SearchDataService, private cookieService: CookieService,
    private searchService: SearchService, private router: Router) { }

  ngOnInit() {
    this.checkEmail();
  }

  deleteCookie(): void {
    // nie działa
    this.cookieService.deleteAll();
    this.searchService.outLog().subscribe(data => { }, error => { console.error(error) });
    this.router.navigate(['/home']);
  }

  checkEmail(): void {
    this.searchService.checkIfLoged().subscribe(
      data => {
        if (data === 'User not logged in') {
          this.router.navigate(['/home']);
          this.displayPopup = true;
        } else {
          this.searchDataService.email = data;
          this.displayPopup = false;
        }
      },
      error => {
        this.displayPopup = true;
        console.error(error);
      }
    );
  }

  onPopupClosed(popup: boolean): void {
    this.displayPopup = popup;
  }

}
