import { Component, OnInit } from '@angular/core';
import { SearchDataService } from '../services/search-data.service';
import { SearchService } from '../services/song.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  displayPopup: boolean;
  email: string;

  constructor(private searchDataService: SearchDataService, private searchService: SearchService) { }

  ngOnInit() {
  }


}
